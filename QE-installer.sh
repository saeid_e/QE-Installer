#!/bin/bash
#
#
#
# This Program is a simple installer for installing QE This 
# will install Dependencies and download QE and finaly compile it
#
# Courtesy of S. Ekrami
# Last Modified in 18 Oct 2016
# Version 0.0
#================================================================
#
echo -e "Welcome to QE installer \n This Installer Will Download And Compile QE-5.4 On Your System \n Last Update : 18 Oct 2016"
echo -e "\x1B[31m Powered by S. Ekrami\e[0m"
read -rsp $'Press any key to continue...\n' -n1 key
"Press any key to continue..."
clear
echo "Installing Dependencies"
echo "please make sure you have internet connections"
read -rsp $'Press any key to continue...\n' -n1 key
"Press any key to continue..."
clear
sudo apt-get update ;
sudo apt-get install build-essential gfortran libblas-dev libblas-test liblapack-dev liblapack-test scalapack-mpi-test libscalapack-mpi-dev libatlas-dev libatlas-base-dev libatlas-test libblacs-mpi-dev blacs-mpi-test fftw3 fftw3-dev fftw3-doc fftw-dev libopenmpi-dev openmpi-bin libhdf5-openmpi-dev -y ;
clear
echo "Install Dependencies Has Been Done!"
sleep 5
clear
echo "Downloading QE"
echo "please enter a directory name for installing QE :" #Make a directory in Home for QE
read D
mkdir -p $HOME/$D ;
echo "$D created in your Home directory!"
sleep 3
wget -c -P $HOME/$D http://www.qe-forge.org/gf/download/frsrelease/211/968/espresso-5.4.0.tar.gz;
tar -xvzf $HOME/$D/espresso-5.4.0.tar.gz -C $HOME/$D ;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/956/tddfpt-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/963/test-suite-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/960/pwcond-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/969/EPW-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/957/GWW-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/972/QE-GPU-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/958/xspectra-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/961/PWgui-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/959/neb-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/962/PHonon-5.4.0.tar.gz;
wget -c -P $HOME/$D/espresso-5.4.0/archive http://www.qe-forge.org/gf/download/frsrelease/211/954/atomic-5.4.0.tar.gz;
echo "Downloading has been done !"
clear
echo "Installing QE"
sleep 5
$HOME/$D/espresso-5.4.0/./configure
sudo make all -C $HOME/$D/espresso-5.4.0
clear
#echo -e "please put this line on your bashrc\nexport PATH=$HOME/$D/espresso-5.4.0/bin:\$PATH"
echo "export PATH=$HOME/$D/espresso-5.4.0/bin:\$PATH" >> $HOME/.bashrc
echo "Installation Has Been Done !"
echo "Good Luck!"
